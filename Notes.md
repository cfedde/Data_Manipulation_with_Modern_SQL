# Notes

## SQLite compilation

* Centos 6 does not have a new enough sqlite3.
* compile from source requires gcc and readline
* https://sqlite.org/2017/sqlite-autoconf-3200000.tar.gz
* After compilation then prepend the directory to your path.

## markdown-to-slides

This was needed on my ubuntu lappy to generate the slides

```
sudo apt install npm
s sodo apt install nodejs-legacy
npm install markdown-to-slides -g
```

## Dev Cycle

The environment script sets up the `$PATH` for the dev cycle.
```
source environment
```

Regenerate the slides ans push to gitlab using the `checkpoint` script.
To refresh the html presentation Slides/slides.html and push changes
to github execute it.

```
checkpoint
```

 
