# Bio:

Chris has spent the last twenty years building, deploying and
supporting what inevitably become the legacy systems of
tomorrow.  Starting in the 80s on HP-UX 5 and continuing through
today in many environments from Internet startups, ISP, Medical
HPC and carrier scale telecom networks. He as had a number of
titles over the years including systems programmer, developer,
systems administrator, architect, technical manager, DBA, devops
engineer. His favorite roles has always been husband and father.

# Expectations:

This will be an alpha test run for this course.  You will be the
first to have a chance to try it and your feedback will help
tune the future of this course.  It has been developed using
standard Linux tools with no special requirements. Some
familiarity with the Linux command line is expected but we can
coach most of that if needed.

# Technical Requirements for this course:

You are expected to bring a laptop that can run linux or can run
a linux virutal machine.
Any linux distro in a virtual box or on your laptop  will be
sufficient.  Even a raspberry pi with proper display and
keyboard  will be enough.  Minimum config might be 1Gbyte ram
with 2Gbyte disk free. Any recent linux distro. 
A default ubuntu virtualbox image will be perfect.  It was
developed on Ubuntu 16.04.2 LTS and tested on  Raspbian
GNU/Linux 8.0 (jessie)

Nearly all work will be done at the command line

Extra packages might include: 
* git
* vim - or some other text editor
* sqlite3 -- version 3.8 or higher
* LebreOffice -- calc was used for some setup  but all the
* resultant CSV files are in the gitlab repo.

Everything else is in the gitlab repo.

# Data Sources

* https://www.census.gov/econ/cfs/2012/cfs_2012_pumf_csv.zip
* https://www.census.gov/econ/cfs/2012/cfs_2012_pum_file_users_guide_App_A%20(Jun%202015).xlsx
* https://www2.census.gov/geo/docs/reference/state.txt
* https://www2.census.gov/geo/docs/reference/codes/files/national_county.txt

# Course Schedule:

## Setup time 

From 9 to 10 will be a setup time to ensure
that everyone has a usable environment.  Pairing up is encouraged. 

## Section One

* Demo -- Full demo of everything we will do run at top speed.
* Load data exercise -- Prep your local database using provided scripts.
* Lab Time

## Section Two
* Simple queries, explore sqlite3, explore the data tables, 
* DDL, DML, DCL. Indexes
* Lab time
* Break

## Section Three

* More complex queries, subqueries, simple functions, aggregate
* functions, joins, group by, order by
* Lab time
* Break

## Section Four
* Common Table Expressions aka CTE,  Recursive CTE, 
* Lab time
* Review -- Re run the demo, Q&A 

End at two if we are done or not.
