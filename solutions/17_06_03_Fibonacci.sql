-- 17.6.3 Fibonacci

with recursive
    fib(i, f1, f2) as ( values(1, 1, 1)
        union all
        select i+1, f1+f2, f1
        from fib
        where i < 10
    )
select * from fib
;
