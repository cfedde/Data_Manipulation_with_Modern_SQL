-- 14.2.5 How many shipments were exported?

drop table if exists country_code;
create table country_code (
    country text,
    code text
)
;

insert into country_code (code, country)
values 
( 'O', 'Other' ),
( 'M', 'Mexico'),
( 'C', 'Canada')
;

.mode column
.header on
select * from country_code;

