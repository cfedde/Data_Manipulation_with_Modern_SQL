-- 14.2.7 Break down total shipment weight by SCTG group

.header on
.mode column
.width 15 10 20 
select sum("SHIPMT_WGHT") TotalWeight, GRP,
    s.Description
from cfs
join "SCTG By Group" using(SCTG)
join (select * from SCTG where "SCTG Group" is not null) s 
    on grp = "SCTG Group"
group by grp
union
select sum("SHIPMT_WGHT") TotalWeight, "Tot", "Total Weight"
from cfs
order by TotalWeight
;

