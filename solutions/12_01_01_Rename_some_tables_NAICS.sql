-- 12.1.1 Rename some tables NAICS

alter table NAICS_North_American_Industry_Classification_System_Codes
    rename to NAICS;
