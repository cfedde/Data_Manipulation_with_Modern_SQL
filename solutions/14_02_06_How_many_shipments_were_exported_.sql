-- 14.2.6 How many shipments were exported?

.mode column
.header on
select count(1) Shipments, Country
from cfs
join country_code on EXPORT_CNTRY = code
group by EXPORT_CNTRY having EXPORT_YN = 'Y'
union
select count(1) Shipments, 'Total'
from cfs
where EXPORT_YN = 'Y'
;

