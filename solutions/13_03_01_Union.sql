-- 13.3.1 Union

.mode columns
.header on

select count(1) as Shipments,
 (select Description from sctg where "SCTG Group" = '01-05') as Description
from cfs where SCTG >= 01 and SCTG <= 05
union
select count(1) as Shipments,
 (select Description from sctg where "SCTG Group" = '15-19') as Description
from cfs where SCTG >= 15 and SCTG <= 19
;
