-- 13.2.2 What state originated the most shipments?

.mode columns
.headers on

select max(shipments), orig_state, (select STATE_NAME from STATE_FIPS where state = orig_state)
    from (
        select orig_state, count(1) as shipments
            from cfs
            group by orig_state
    )
;
