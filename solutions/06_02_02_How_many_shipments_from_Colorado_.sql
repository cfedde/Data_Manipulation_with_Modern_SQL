-- 6.2.2 How many shipments from Colorado?

.schema
.mode line
select * from state_fips where state_name like 'Colorado' limit 1;
select * from cfs where orig_state = '08' limit 1;
select count(1) from cfs where orig_state = '08';
