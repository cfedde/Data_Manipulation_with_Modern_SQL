-- 5.3.17 Look at data from the other tables we loaded.

.mode line
select * from NAICS_North_American_Industry_Classification_System_Codes
limit 1
;
