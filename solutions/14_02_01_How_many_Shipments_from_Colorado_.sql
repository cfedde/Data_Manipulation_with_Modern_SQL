-- 14.2.1 How many Shipments from Colorado?

.mode column
.header on

select count(1) as Shipments, state_name from cfs
join state_fips s on state = orig_state
where s.stusab = 'CO'
;
