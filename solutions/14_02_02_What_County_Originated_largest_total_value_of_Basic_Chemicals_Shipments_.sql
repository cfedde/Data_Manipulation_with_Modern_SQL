-- 14.2.2 What County Originated largest total value of Basic Chemicals Shipments?

.echo on

create temporary table sctg_grp as 
    select * from "SCTG By Group" 
    join SCTG using(sctg)
;

create temporary table bc_sctg as 
    select sctg from sctg_grp
    where grp in (
        select grp from sctg_grp where description = 'Basic Chemicals'
    )
;

create temporary table tsv as 
    select sum(SHIPMT_VALUE) Total_Shipment_Value,
        ORIG_STATE, ORIG_MA, ca.*
        from cfs
        join CFS_Areas ca on orig_state = ca.state and ORIG_MA = ca.ma 
        where cfs.sctg in (select sctg from bc_sctg )
        group by ORIG_STATE, ORIG_MA
        order by Total_Shipment_Value
;

.mode column
.header on
select max(Total_Shipment_Value), Description from tsv
;

