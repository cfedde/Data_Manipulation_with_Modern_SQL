-- 13.2.1 How many shipments from Colorado

.mode columns
.header on

select count(1) as shipments
    from cfs
    where orig_state = (
        select state from state_fips
            where state_name like 'Colorado'
    )
;
